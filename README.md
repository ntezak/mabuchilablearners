
Here we only discuss the structure and usage details of the MabuchiLabLearners
git-repository.
For our editable shared README see the relevant [README-
hackpad](https://hackpad.com/README-2uf1wBeT05j).

# Files and Directories

## The top-level directory

Current listing

In[1]:

```
!ls -lt .
```


    total 40
    -rw-r--r--  1 nikolas  staff  8722 Sep  2 10:56 README.ipynb
    -rw-r--r--  1 nikolas  staff  4701 Sep  2 10:49 README.md


The current files are:

- `README.ipynb` -- Used to generate the README.md via the `ipython nbconvert`
command, can be seen at end of notebook.
- `README.md` -- Actual README file for the repository.


## The session directories

For your very own session, please create a subdirectory in this repository named
`session_XY_your_topic_with_lowercase_and_underscores`,
e.g. for **session 1** on quantum probability and qutip, I would create a folder
as

    mkdir session_01_quantum_probability_and_qutip

Within each such folder, there should be a brief `README.md` markdown file. This
file should explain the role of all other files present in your session's
subdirectory under a special `Files` heading. In particular, please indicate if
any files are automatically generated from other files. If you have code
examples in your session, please provide details on the necessary computational
environment/its dependencies.

As an example, please consult my `README.md` for session 1.
Other than that, if you have a single master file with all your notes, please
name it one of

    Notes.ipynb
    Notes.nb
    Notes.tex
    Notes.md
    Notes.rst

depending on what format you are using.
It is ok for more than one such file to exist. In this case, we will assume by
default, that any LaTeX file was generated from whatever other formats where
present. In the absence of a tex-file, we assume that `.md` and `.rst` files
were generated from any existing `.ipynb` notebook. It doesn't make much sense
to have both an `.rst` and a `.md` file, so please don't do that without a very
good reason.

All this will ultimately make it easier for us to compile all notes into a
single wiki/webpage/whatever.

# Using GIT

## Cloning the repository

For a proper intro to using git, please see one of the relevant tutorials
referenced in the relevant hackpad.
Here we will only explain the minimal set of things to get started.

0. First, you must have `git` installed. Typing `git --version` in the command
line will reveal whether that is the case. If it isn't, please download and
install it from the official [git-scm website](http://git-scm.com/).
1. You need a user account at [bitbucket](http://bitbucket.org).
2. You need to send [me and email](mailto:ntezak@stanford.edu) to request access
to our repository.
3. You need to clone this repository to your local machine. First `cd` in to a
directory in which you store your projects as subdirectories.
    Then, run:

        git clone
https://YOURUSERNAMEHERE@bitbucket.org/ntezak/mabuchilablearners.git

    You should now have a newly created directory named `mabuchilablearners` in
the current directory.
    `cd` into it and list its detailed contents ordered by the file modification
date:

        cd mabuchilablearners
        ls -lt .

That's basically it! The repository is now on your machine.

## Adding local files to the repository

Say you have created some files in a new sub directory specifically for your
presentation/lecture:

- `mabuchlilablearners/`
    - `README.md`
    - `README.ipynb`
    - `session_01_quantum_probability_and_qutip/`
        - `README.md`
        - `Notes.ipynb`
        - `Notes.rst`

If I now intend to add the new files to the repository, I need to run:

    cd session_01_quantum_probability_and_qutip
    git add README.md Notes.ipynb Notes.rst
    cd ..

***Note that you do not have to add a directory explicitly!***
It will get automatically added once you have added one of its files.

To commit these changes and store a current *revision* of the repository (i.e.
the current state of all *tracked* files), now run:

    git commit -m "Added material for session 1: Quantum Probability and QuTiP"


## Syncing your local repository with the remote repository on bitbucket

Let's say I have worked on my notes and in that process committed a series of
revisions to my local repository.
Now, I wish to add my changes to the remote repository.
To do this, I will always first need to `pull` the remote updates that may have
been made by other users:

    git pull


This may or may not lead to git having to perform a merge of changes. The status
message of the above command will notify you.
**If some unsupervised/automatic merge occurred, you may have to commit the
merged state:**

    git commit -m "Merged local and remote changes"

**If some merge conflicts have arisen, you will need to resolve these manually.
Use google or ask a labmate on how to do this.**

Otherwise, you are now ready to `push` your changes back:

    git push



***IGNORE THIS:*** Export this document to a markdown format

In[1]:

```
!ipython nbconvert --to markdown README.ipynb
```


    [NbConvertApp] Using existing profile dir: u'/Users/nikolas/.ipython/profile_default'
    [NbConvertApp] Converting notebook README.ipynb to markdown
    [NbConvertApp] Support files will be in README_files/
    [NbConvertApp] Loaded template markdown.tpl
    [NbConvertApp] Writing 4701 bytes to README.md

