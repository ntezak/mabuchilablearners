% Created 2013-09-06 Fri 14:58
\documentclass{scrartcl}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{graphicx}
\author{Nikolas Tezak}
\date{\today}
\title{Notes}
\hypersetup{
  pdfkeywords={},
  pdfsubject={},
  pdfcreator={Emacs 24.3.1 (Org mode 8.0.7)}}
\begin{document}

\maketitle
\tableofcontents



\section{Quantum Probability}
\label{sec-1}

\subsection{Intro}
\label{sec-1-1}
Most intros to quantum mechanics will teach mostly pure state
quantum theory of closed systems.  In that framework, all
uncertainty in the outcome of a measurement is purely due to quantum
theory.  Clearly, this is not the most general case as it does not
allow for any imperfection in our prior knowledge, as well as either
our ability to either perfectly isolate our system or alternatively
our capability to detect all information leaking out of it.

In generalizing from pure states to states described by density
operators $\rho$ all these issues can be adressed.  In this more
general framework pure states can be characterized as being the
extremal points of the convex set of possible density operators. Or,
equivalently, they are entropy minimizing states and thus, in some
sense describe A situation of maximal knowledge.
\subsection{Relationship between classical and quantum probability}
\label{sec-1-2}
This part will be based on the first two lectures of Hideo's and
Y. Yamamoto's class. See the references section below for a link to
the course website.


\begin{figure}[htb]
\centering
\includegraphics[width=.9\linewidth]{./figures/HMVisualization.png}
\caption{\label{fig:hmvis}Hideo's visualization}
\end{figure}


What Hideo meant to visualize here is that classical physics and
information theory and their quantum equivalents are not
generalizations of each other. Though there exists a lot of overlap,
there are things in either case that are fundamentally incompatible
with the other respective theory. Think, e.g., of the no-cloning
theorem, quantum interference, entanglement.

It turns out, however, that any particular classical probability model
can be fully embedded into a more general quantum probability
framework. This is one of the key things we'll see today.

Finally, in classical probability theory there exists an underlying,
all-encompassing event space $\Omega$, which quite naturally leads to
boolean logic for events. This structure does not exist in quantum
theory. The closest one can get is by choosing a particular complete
subset of commuting observables of a system, whose mutual eigenbasis
then forms something analog to the classical event space. However, it
is an inherent feature of this construction, that there will always
exist observables whose potential outcomes cannot be part of that same
underlying event space and whose joint measurement with the other
observables is ill-defined.

\subsection{Random variables and observables}
\label{sec-1-3}
Given an underlying event space $\Omega$, a random variable is
characterized as assigning some numerical value to whichever event
occurred, e.g., a real valued random variable is a map

\begin{align}
X:\Omega \to \mathbb{R}, \omega \mapsto X(\omega).
\end{align}
For simplicity, we will only discuss finite discrete $\Omega$.

As an example, consider rolling a die. Neglecting all degrees of
freedom apart of its orientation, the underlying abstract event space
has 6 elements, which are abstractly given as $\Omega = \{\omega_1,
   \omega_2, \dots, \omega_6\}$.  Generally, when we speak of an
\textbf{\textbf{event}} we will mean a whole subset $A\subseteq \Omega$, but this
subset may have a single element.  Random variables can be added,
subtracted, multiplied, etc. by doing these operations pointwise,
e.g. $(X + Y)(\cdot) = X(\cdot) + Y(\cdot)$ as is typical notation for
maps.

The most obvious random variable would be the number of dots on the
upwards facing side:
\begin{align}
X(\omega_j) = j,\quad j=1,2,\dots, 6
\end{align}

So, in this particular case, when the random variable is
\textbf{\textbf{one-to-one}}, we might as well identify the probability event space
with the range of the random variable $X$. Note, however, that in
principle, we could have chosen any other one-to-one random variable
for this purpose.
We have, so far, not mentioned how to compute probabilities. To this
end, we introduce a probability mass function:
\begin{align}
m:\Omega \to \mathbb{R}, \omega \mapsto m(\omega) \in [0,1]
\end{align}
with $\sum_\omega m(\omega) = 1$.

We can overload the notation to define the probability of events $m(A) := \sum_{a\in A} m(a)$.
Now, the expectation value of $X$ can be defined as:
\begin{align}
\langle X \rangle = \sum_{ \omega } X (\omega) \; m(\omega) = \sum_{x} x \; m(X^{-1}(x))
\end{align}

Here, $m_X(x):= m(X^{-1}(x))$ is the induced probability measure on
the range of $X$, which demonstrates how we can use random variables
to remove the necessity for an abstract underlying event space. There
are some technicalities: When $X$ isn't one-to-one, the induced
measure cannot be used for evaluating expectations of arbitrary other
random variables defined on the original $\Omega$. As one can easily
verify, they need to be constant on all level sets of $X$.

An interesting representation of discrete-valued random values is in terms of indicator functions:
\begin{align}
\forall A \subseteq \Omega: \chi_A(\omega) = \begin{cases} 1 & \omega \in A \\ 0 & {\rm else} \end{cases}
\end{align}

Then, we can decompose $X$ as
\begin{align}
X = \sum_{x \in X(\Omega)} x \; \chi_{X^{-1}(x)},
\end{align}

where $X^{-1}(x)$ is the levelset of $x$ under $X$.
Using the linearity of the expectation value, we can find
\begin{align}
\langle X \rangle =  \sum_{x} x \; \langle \chi_{X^{-1}(x)} \rangle
\end{align}
from which follows $\langle \chi_{X^{-1}(x)} \rangle = m(X^{-1}(x))$,
which is quite intuitive.


Now, compare the indicator function decomposition with a quantum
mechanical observable represented in its eigenbasis:
\begin{align}
\hat{X} = \sum_{x\in {\rm spec}(\hat{X})} x \; \hat{\Pi}_x,
\end{align}
where $\hat{\Pi}_x$ is the projector onto the relevant eigenspace.

In this case, the expectation value for some state $\hat{\rho}$ is evaluated as:
\begin{align}
\langle \hat{X}\rangle_{\hat{\rho}} = \sum_{x\in {\rm spec}(\hat{X})} x \; \langle \hat{\Pi}_x\rangle_{\hat{\rho}}
\end{align}
which looks so similar to above, that we are lead to introduce the notation
\begin{align}
m_{\hat{\rho},\hat{X}}(x) := \langle \hat{\Pi}_x\rangle_{\hat{\rho}}
\end{align}
as again, the probability of outcome $x$.

And it is this analogy, that allows us to find a straightforward way to embed any classical probability model into a quantum version.
Represented in its eigenbasis, an observable is a diagonal matrix.
\begin{align}
D(\hat{X}) = 
\begin{pmatrix} 
x_1 & 0 & 0 & \cdots & 0\\ 
0 & x_2 & 0 & \cdots & 0 \\ 
\vdots &  & \ddots & & \vdots \\ 
\vdots &  &  & x_{n-1} & 0 \\     
0 & &\cdots &0 & x_n 
\end{pmatrix}
\end{align}
And similarly, we could represent any random variable as a diagonal matrix
\begin{align}
D(X):=
\begin{pmatrix} 
X(\omega_1) & 0 & 0 & \cdots & 0\\ 
0 & X(\omega_2) & 0 & \cdots & 0 \\ 
\vdots &  & \ddots & & \vdots \\ 
\vdots &  &  & X(\omega_{n-1}) & 0 \\     
0 & &\cdots &0 & X(\omega_n)
\end{pmatrix}
\end{align}

In this representation one can still perform algebraic operations
between different random variables (the algebra is commutative,
because all random variables have diagonal representations).  This
embedding, however, shows that quantum probability \textbf{generalizes}
classsical probability theory, because there exist, in principle,
observables that aren't diagonal in the above basis.

Although I have proved nothing formally here, the analogy works both
ways: Only a set of commuting observables, i.e., simultaneously
diagonalizable operators, can be used to define a consistent classical
probability model for the joint measurements. Each observable then
corresponds to a random variable.  This isn't to say that one cannot
perform non-commuting measurements, but such measurements will then
have a necessary back-action on the system and the outcomes will thus
be order-dependend cannot therefore be called a \textbf{simultaneous}
Realization of the observable's random variables.  In a way, any
orthonormal basis for the Hilbert space induces a probability model
and any state $\hat{\rho}$ of the system supplies the corresponding
probability mass function via its diagonal matrix elements when
represented in that basis. This suggests that, an empirically observed
distribution of measurement outcomes, even for a complete set of
observables, does not allow us to reconstruct the full pre-measurement
state, only its diagonal elements.
\subsection{Composite systems}
\label{sec-1-4}

Finally, note that both for quantum state spaces and classical
probability spaces, we get an exponential scaling of the overall
dimension with the number of subsystems, because we always need to
track of every possible combination of subsystem states.

This extension to a composite space is usually called Tensor
product.  For two spaces $\mathcal{H}_j = {\rm
   span}\,(\{|k\rangle_j, k = 1,2\dots,d_j\}),\quad j=1,2$ we can form
the tensor product space as 
\begin{align} 
\mathcal{H}_1 \otimes \mathcal{H}_2 = {\rm span}\,(\{|k \rangle_1|l \rangle_2, k = 1,2\dots,d_1; l = 1,2\dots,d_2\})
\end{align}

In classical probability, a general joint probability measure is
specified by assigning numbers to all possible combinations:
$m_{1,2}(\omega_1, \omega_2)$

Given a state of a joint system $\hat{\rho}_{AB}$, the expectation value
of any observable's depending purely on one of the subsystems, say
$A$ can be computed in terms of the reduced density matrix obtained
by a \emph{partial trace} operation:

\begin{align}
\langle \hat{X}_A \rangle & = {\rm Tr} \hat{\rho}_{AB} (\hat{X}_A\otimes \hat{1}_{B})\\
& = \sum_{k,l=1}^{d_A}\sum_{m,n=1}^{d_B} (\hat{\rho}_{AB})_{km,ln} (\hat{X}_A\otimes \hat{1}_{B})_{ln,km} \\
& = \sum_{k,l=1}^{d_A}\sum_{m,n=1}^{d_B} (\hat{\rho}_{AB})_{km,ln} (\hat{X}_A)_{lk}\delta_{n,m} \\
& = \sum_{k,l=1}^{d_A}\sum_{m=1}^{d_B} (\hat{\rho}_{AB})_{km,lm} (\hat{X}_A)_{lk} \\
& = \sum_{k,l=1}^{d_A}\left[\sum_{m=1}^{d_B} (\hat{\rho}_{AB})_{km,lm}\right] (\hat{X}_A)_{lk} \\
& = \sum_{k,l=1}^{d_A} (\hat{\rho}_{A})_{kl} (\hat{X}_A)_{lk} \\
& = {\rm Tr}_A \hat{\rho}_{A}\hat{X}_A
\end{align}

This is similar to classical probability where expecations of
subsystem random variables can be computed by the marginal
distribution:
\begin{align}
\langle f(x) \rangle & = \sum_{x,y} m(x,y) f(x)\\
& = \sum_{x} \left[\sum_y m(x,y)\right] f(x)\\
& = \sum_{x} m(x) f(x)
\end{align}
\subsection{Conditional expectations/ Bayes' rule}
\label{sec-1-5}

In classical probability, for $A, B \subset \Omega$ we can form the
conditional probability measure

\begin{align}
m(A|B) := \frac{m(A \cap B)}{m(B)}
\end{align}
which gives the probability that $A$ occurs, given that $B$
definitely occured.



In quantum probability our concept of an \emph{event space} is always
basis dependent or equivalently dependent on a particular choice of
complete set of commuting observables.  Thus, given two observables
$\hat{A}$ and $\hat{B}$ with spectra $\{a_j\}$ and $\{b_k\}$
respectively, and starting from a state $\hat{\rho}$ we may ask,
what is the conditional expectation $p(a|b)$ to have measured
$\hat{A}$ to be $a$ given that the measurement result for $\hat{B}$
was $b$.

The answer looks somewhat analogous to the above:
\begin{align}
p(a|b)_{\rm joint} = \frac{{\rm Tr}\, \hat{\Pi}_a \hat{\Pi}_b \hat{\rho}}{ {\rm Tr} \hat{\Pi}_b \hat{\rho} }
\end{align}

Note that if $\hat{A}$ and $\hat{B}$ did not commute, then in
general $\hat{\Pi}_a$ and $\hat{\Pi}_b$ won't commute and this
would mean that $\hat{\Pi}_a \hat{\Pi}_b$ isn't hermitian and thus
the above defined probability measure wouldn't be real-valued.

Note, however, that this is only important insofar we are talking
about joint measurement outcomes. If we were instead to perform
sequential measurements of (generally non-commuting operators) we
could express the probability for a measurement of $\hat{A}$ to
yield $a$ \textbf{after} a measurement of $\hat{B}$ gave a result $b$ to
be

\begin{align}
p(a|b)_{\rm seq} = \frac{{\rm Tr}\, \hat{\Pi}_a \hat{\Pi}_b\hat{\rho} \hat{\Pi}_b}{ {\rm Tr} \hat{\Pi}_b \hat{\rho}}
\end{align}

\subsubsection{{\bfseries\sffamily NEXT} Update/improve this and add Dmitri's comment}
\label{sec-1-5-1}
The unconditional evolution under measurement (i.e., averaging
over all outcomes) produces a genuinely different result than in
classical probability. Stress these differences more!!!
\subsection{Homework assignments (all using QuTiP)}
\label{sec-1-6}


\begin{enumerate}
\item Show that the partial trace over one subsystem of a bell-state
results in a mixed state for the second subsystem.

\item Find out what the commands `spre` and `spost` are good for, and how exactly they work.

\item Plot the Q- and the Wigner functions for

\begin{enumerate}
\item the vacuum

\item a coherent state  $|\alpha\rangle$ where $\alpha = 1 + i$

\item a cat state $|\alpha\rangle + |-\alpha \rangle$ where $\alpha=1,2,4$

\item a fock state with 10 photons
\end{enumerate}

\item Consider two coupled harmonic oscillators with mode operators $\hat{a}$ and $\hat{b}$.

Take the full Hamiltonian (in a rotating frame to be)

\begin{align}
\hat{H} = \Delta \hat{a}^{\dagger}\hat{a} + \gamma \left(\hat{a}^{\dagger}\hat{b} + \hat{a}\hat{b}^{\dagger}\right) + \eta \left(\hat{a} + \hat{a}^{\dagger}\right)\hat{b}^{\dagger}\hat{b}
\end{align}

For the two parameter configurations $\Delta = 5, \gamma = 1, \eta = 0$ and $\Delta = 5, \gamma = 0, \eta = 1$ do the following:

\begin{enumerate}
\item Starting in an initial state of
$|\psi_0\rangle := |\psi_0^{(a)}\rangle |\psi_0^{(a)}\rangle
         = |\alpha\rangle |\beta\rangle$ with $\alpha = 2, \beta = 3$,
evolve the system under its Hamiltonian for 5 time
units. Plot the Q-function of both modes at every full time
unit.

\item At t=5 the photon number is measured. For all possible
measurement outcomes in the range of $0$ to $10$, plot the
post-measurement Q-Function of $\hat{a}$.

\item BONUS Can you find operators $\hat{E}^{(a)}_j(t;\beta)$ on
$\hat{a}$'s Hilbertspace only, that for each measurement
outcome $j$ of $\hat{b}$'s photon number give the ultimate
post measurement state
\end{enumerate}
\end{enumerate}


\begin{align}
| (\psi_t^{(a)} | j)\rangle = \frac{ \hat{E}^{(a)}_j(t;\beta) | \psi_0^{(a)}\rangle }{\sqrt{\langle \psi_0^{(a)}  |\hat{E}^{(a)\dagger}_j(t;\beta)  \hat{E}^{(a)}_j(t;\beta) | \psi_0^{(a)}\rangle}}
\end{align}
Show that $\sum_{j=0}^\infty \hat{E}^{(a)\dagger}_j(t;\beta)\hat{E}^{(a)}_j(t;\beta) = \hat{1}^{(a)}$.

Compute these operators for $j=0, 2, 10$ and verify their
action by comparing it to the full evolution.

Hint: Factor the coherent state on $\hat{b}$ using the displacement operator.


\subsection{References}
\label{sec-1-7}
Most of the stuff discussed here was based on Hideo's course:

\begin{itemize}
\item Hideo's and Y. Yamamoto's \href{http://www.stanford.edu/~rsasaki/AP202/AP202.html}{Lecture notes for AP202 (taught in Winter 2012)}
\item A more philosophical perspective \href{http://plato.stanford.edu/entries/qt-quantlog/}{Quantum Logic and Probability Theory}
\end{itemize}
% Emacs 24.3.1 (Org mode 8.0.7)
\end{document}