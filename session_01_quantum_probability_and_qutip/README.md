# Session 1: Intro to Quantum Probability and QuTiP #

This session will cover some aspects of basic quantum probability and in
particular the similarities and differences it shares with classical
probability. This first part of the lecture will be based on Hideo's course on
Quantum Probability (AP202).
We will then interactively explore some of these concepts by using QuTiP.

The homework assignment will also involve solving some problem in QuTiP.

# Files and Directories

The current files are:

- `README.md`
- `Notes.org` -- source org-mode file to generate latex source from with emacs
- `Notes.tex` 
- `Notes.pdf` -- notes generated from tex file
- `Homework_1.ipynb` -- IPython notebook with the homework solutions.
  To preview this notebook rendered visit the [nbviewer app](http://nbviewer.ipython.org/urls/bitbucket.org/ntezak/mabuchilablearners/raw/06f5daea4c96414cf15fa82a2865901910787f10/session_01_quantum_probability_and_qutip/Homework_1.ipynb)
- `QuTiP-notes.ipynb` -- IPython notebook with some simple examples to get started with QuTiP
  To preview this notebook rendered visit the [nbviewer app](http://nbviewer.ipython.org/urls/bitbucket.org/ntezak/mabuchilablearners/raw/9d61afd2bb2e628ec3cefd77a41de6cad180d340/session_01_quantum_probability_and_qutip/QuTiP-Notes.ipynb)
- `HideoNotes/HM*.pdf` -- These are the notes of Hideo's four first AP202 lectures.
- `figures/HMVisualization.png` -- Image extracted from Hideo's notes

For further introduction to QuTiP as well as python and IPython for scientific computing and plotting, 
please consult the [excellent notebooks created by Robert Johansson](http://jrjohansson.github.io/).
